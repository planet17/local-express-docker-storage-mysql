# Local Express: Main storage (MySQL) #

https://bitbucket.org/planet17/local-express-docker-storage-mysql


-------------------


Separated main storage container for Test Specification.

Docker container with MySQL storage. That application is part of a few docker containers.
For more details about whole project look the repository
[Bridge](https://bitbucket.org/planet17/local-express-docker-core-bridge).



## Environment ##


-------------------

    Docker: *
    MySQL: 5.7.30

-------------------


## Installation ##

After cloning that project create local `.env`

```shell
cp .env.example .env
```

If something from `.env` had been changed in another project then it must have the same value here is.
After syncing of `.env` file run following:

```shell
docker-compose up -d
```
